package kz.test.spark

import org.apache.spark.sql.SparkSession

object Main {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder.appName("Spark test").getOrCreate()
    import spark.implicits._
    val df = spark.read.textFile("README.md")
    val words = df.flatMap(_.split(" "))
    words.createOrReplaceTempView("words")
    val result = spark.sql("select value word, count(*) qty from words group by value order by 2 desc")
    result.repartition(1).write.csv("test/sql")
    spark.stop()
  }

}
